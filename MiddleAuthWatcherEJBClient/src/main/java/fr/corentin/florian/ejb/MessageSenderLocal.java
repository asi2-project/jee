package fr.corentin.florian.ejb;

import fr.corentin.florian.model.UserModel;

public interface MessageSenderLocal {

    void sendMessage(String message);
    void sendMessage(UserModel user);
}
