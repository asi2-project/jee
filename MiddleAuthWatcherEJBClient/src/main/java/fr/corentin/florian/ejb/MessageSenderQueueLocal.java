package fr.corentin.florian.ejb;

import fr.corentin.florian.model.UserModel;

import javax.ejb.Local;

@Local
public interface MessageSenderQueueLocal {
    void sendMessage(String message);
    void sendMessage(UserModel user);
}
