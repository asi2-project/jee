package fr.corentin.florian.data;

import fr.corentin.florian.model.UserModel;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@ApplicationScoped
public class UserRepository {

    @PersistenceContext
    private EntityManager em;

    public void add(UserModel um) {
        em.persist(um);
    }

    public UserModel findById(Long id) {
        return em.find(UserModel.class, id);
    }

    public UserModel findByCredentials(String login, String password) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserModel> criteria = cb.createQuery(UserModel.class);
        Root<UserModel> user = criteria.from(UserModel.class);

        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(cb.equal(user.get("login"), login));
        predicates.add(cb.equal(user.get("password"), password));
        criteria.select(user).where(predicates.toArray(new Predicate[]{}));

        return em.createQuery(criteria).getSingleResult();
    }

    public List<UserModel> findAllOrderedByName() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<UserModel> criteria = cb.createQuery(UserModel.class);
        Root<UserModel> user = criteria.from(UserModel.class);

        criteria.select(user).orderBy(cb.asc(user.get("name")));

        return em.createQuery(criteria).getResultList();
    }
}
