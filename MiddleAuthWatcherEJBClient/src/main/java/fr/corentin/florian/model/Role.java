package fr.corentin.florian.model;

public enum Role {
    NONE,
    ADMIN,
    UNKNOWN
}
