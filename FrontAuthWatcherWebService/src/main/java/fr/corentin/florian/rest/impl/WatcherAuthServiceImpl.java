package fr.corentin.florian.rest.impl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import fr.corentin.florian.ejb.MessageReceiverSyncLocal;
import fr.corentin.florian.ejb.MessageSenderLocal;
import fr.corentin.florian.model.Role;
import fr.corentin.florian.model.UserModel;
import fr.corentin.florian.rest.WatcherAuthService;

import javax.ejb.EJB;

public class WatcherAuthServiceImpl implements WatcherAuthService {

    @EJB
    MessageSenderLocal sender;

    @EJB
    MessageReceiverSyncLocal receiver;

    @Override
    public String authenticate(String auth) {
        UserModel user;

        Gson gson = new Gson();
        user = gson.fromJson(auth, UserModel.class);

        sender.sendMessage(user);


        UserModel receive = receiver.receiveMessage();
        JsonObject result = new JsonObject();

        if (receive == null) {
            result.addProperty("login", user.getLogin());
            result.addProperty("validAuth", false);
            result.addProperty("role", Role.NONE.toString());

        } else {
            result.addProperty("login", receive.getLogin());
            result.addProperty("validAuth", receive.getRole() != null);
            result.addProperty("role", receive.getRole() == null ? "" : receive.getRole().toString());
        }

        return result.toString();
    }
}
