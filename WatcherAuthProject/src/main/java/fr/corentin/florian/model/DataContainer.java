package fr.corentin.florian.model;

import fr.corentin.florian.data.UserRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.NoResultException;

@Stateless
public class DataContainer {

    @Inject
    UserRepository userRepository;

    public Role checkUser(UserModel user) {
        try {
            UserModel u = userRepository.findByCredentials(user.getLogin(), user.getPassword());
            return u.getRole();
        } catch (NoResultException e) {
            return Role.UNKNOWN;
        }

    }
}
