package fr.corentin.florian.ejb;

import fr.corentin.florian.model.UserModel;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.*;

@Stateless
public class MessageReceiverSync implements MessageReceiverSyncLocal {

    @Inject
    JMSContext context;

    @Resource(mappedName = "java:/jms/queue/watcherqueue")
    Queue queue;

    @Override
    public UserModel receiveMessage() {
        JMSConsumer consumer = context.createConsumer(queue);
        UserModel u=consumer.receiveBody(UserModel.class, 1000);
        consumer.close();
        return u;
    }
}
